var creditCardDetails = new Array();

class creditCardRecord {
    constructor(cardName, cardNumber, expireDate, CVV, country) {
        this.cardName = cardName;
        this.cardNumber = cardNumber;
        this.expireDate = expireDate;
        this.CVV = CVV;
        this.country = country;
    }
}

var countries = [
    { Name: "Austria" },
    { Name: "Belgium"},
    { Name: "Bulgaria" },
    { Name: "Cyprus" },
];

function addCard(event) {
    event.preventDefault();
       
    var isCardNumberValid = new RegExp("[0-9]+");

    const isBannedCountry = countries.some(element => {
        if (element.Name === form.country.value) {
            return true;
        }
    });

    const cardAlreadySubmitted = creditCardDetails.some(element => {
        if (element.cardNumber === form.cardNumber.value) {
            return true;
        }
    });

    if(form.cardName.value == "" || form.cardNumber.value == "" || form.expireDate.value == ""|| form.CVV.value == ""|| form.country.value == ""){
        Swal.fire("Invalid form");
    }

    else if (!isCardNumberValid.test(form.cardNumber.value)) {
        Swal.fire("Invalid card number");

    } else if (isBannedCountry){
        Swal.fire("This Country is banned");

    } else if (cardAlreadySubmitted){
        Swal.fire("This credit card was already submitted");
        
    } else {
        var submittedDetails = new creditCardRecord(form.cardName.value,form.cardNumber.value,form.expireDate.value,form.CVV.value,form.country.value);
        creditCardDetails.push(submittedDetails);

        var rows = "";
        $.each(creditCardDetails, function(){
            rows = "<tr><td>" + this.cardName + "</td><td>" + this.cardNumber + "</td><td>" + this.expireDate + "</td><td>" + this.CVV + "</td><td>" + this.country + "</td></tr>";
        });
        $( rows ).appendTo( "#itemList tbody" );
        form.reset();
    }
}

//----Banned Country functionality----

function loadBannedCountryList() {
    var items = "";
    $.each(countries, function(){
        items += "<ul><li>" +  this.Name + "</li></ul>";
    });
    $( items ).appendTo( "#demo" );
}

function addCountry() {
    var addCountry = document.getElementById("addCountryName").value;
    countries.push({
        Name: addCountry,
    });
    var item = "";
    item += "<ul><li>" +  addCountry + "</li></ul>";
    $( item ).appendTo( "#demo" );
    countryForm.reset();
}


function deleteCountry() {
    var countryName = document.getElementById("deleteCountryName").value;
    let removeCountryName = countries.filter(obj => obj.Name == addCountry);
    countries.pop(removeCountryName);
    $('li:contains("'+ countryName+'")').remove();
    countryForm.reset();
}